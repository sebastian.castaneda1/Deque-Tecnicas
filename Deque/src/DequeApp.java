import java.net.SocketTimeoutException;
import java.util.ArrayDeque;
import java.util.Deque;

public class DequeApp {
    public static void main(String... quirama){
        Deque<String> lista = new ArrayDeque<>();
        lista.add("Este es el primer item agregado");
        lista.add("Y este el segundo");
        lista.add("Este será el tercero");

        lista.addLast("Esto se agregara al final");
        lista.addFirst("Y esto al inicio");
        lista.addLast("Y esto no se vera");
        lista.addFirst("Esto menos");

        lista.removeLast(); //elimnara el ultimo objeto que se encuentre en la cola
        lista.removeFirst();//elimnara el primer objeto que se encuentre en la cola

        for(String elemento : lista){
            System.out.println(elemento);
        }
    }
}
